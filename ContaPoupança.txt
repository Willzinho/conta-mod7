public class ContaPoupança extends Conta {
	
	private int aniversario;
	private double juros;
	private int dataDoSaque;
	
	public ContaPoupança(int numero, int agencia, String banco, double saldo, double deposito, double saque, int aniversario, double juros) {
		super(numero, agencia, banco, saldo, deposito, saque);
		this.aniversario = aniversario;
		this.juros = juros;
	}
	
	
	public double getSaldo() {
		if (this.dataDoSaque <=  31 && this.dataDoSaque <= this.aniversario) {
			return ((this.saldo + this.deposito - getSacar()) * this.juros) + this.saldo + this.deposito - getSacar();
		}
		else {
			return this.saldo;
		}
		
	}


	@Override
	public double getDepositar() {
			return this.deposito;
		}
	


	@Override
	public double getSacar() {
		if (this.saque <= this.saldo + this.deposito) {
			return this.saque;
		}
		else {
			return this.saque = 0;
		}
		
	}

}